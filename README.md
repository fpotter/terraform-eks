# Terraform-EKS (copy)

Create an EKS cluster using Terraform on GitLab CICD, and connect it to GitLab using the kubernetes agent.

Based on the [terraform-eks project](https://gitlab.com/gitlab-org/configure/examples/gitlab-terraform-eks.git) described in [GitLab documentation](https://docs.gitlab.com/ee/user/infrastructure/clusters/connect/new_eks_cluster.html) with some input from a [friend](https://gitlab.com/sandlin/aws/gitlab-terraform-eks).

I made some modifications from the example project in `gitlab-org` and associated docs:

1. Added some bits to enable testing from a local Docker container
1. Removed the version constraint from `eks.tf`
1. Deleted the lockfile and rerun `terraform init`
1. Added to the AWS user/role policy:
    - `iam:PutRolePolicy`
    - `iam:GetRolePolicy`
    - `kms:TagResource`
    - `kms:CreateKey`
    - `kms:CreateAlias`
    - `iam:CreatePolicy`
    - `iam:CreateOpenIDConnectProvider`
    - `kms:ListAliases`
    - `iam:TagOpenIDConnectProvider`
    - `iam:GetPolicy`
    - `kms:DeleteAlias`
    - `iam:ListPolicyVersions`
    - `iam:GetOpenIDConnectProvider`
    - `iam:DeletePolicy`
    - `iam:DeleteOpenIDConnectProvider`
    - `iam:DeleteRolePolicy`
1. Changed `module.eks.cluster_id` to `module.eks.cluster_name` 2x in `data.tf`

That's where I left off. The last change enabled `terraform apply` to completely create the cluster from a local system, but it failed to install the agent because the cluster was unreachable. Same result with `kubectl`.

On GitLab CICD, the `data.tf` change resulted in a "couldn't find resource" error ([job](https://gitlab.com/fpotter/terraform-eks/-/jobs/6442967788)).
